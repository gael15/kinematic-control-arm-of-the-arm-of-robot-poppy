#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 11 13:25:54 2021

@author: guetin gael
"""
#Librairies
from math import *
import pypot.vrep.io as vrep_io
import numpy as np
from time import *
import vrep_poppy
import pypot
import matplotlib.pyplot as plt

# #Librairies json pour la commande du real robot
# import json
# #from lib import real_membres

# # ouverture de la configuration du robot (ici jambe gauche) grace au JSON
# with open('poppy_left_arm_config.json', 'w') as a:
# #config = json.dump(real_membres.poppy_left_leg_config, f, indent=2)

# # Creation d'une instance (robot) en fonction du JSON
# poppy = pypot.robot.from_json('poppy_left_arm_config.json')

# # Debut de la synchronisation entre les moteur qui composent le robot et le code
# poppy.start_sync()

# # declaration du membre reel sur le robot
# l_arm = ['l_shoulder_y', 'l_shoulder_x', 'l_arm_z', 'l_elbow_y'] # groupe de moteur

# jambe_gauche = real_membres.RealMembre(l_arm, poppy)

# # reset des moteurs
# jambe_gauche.reset_membre(['l_ankle', 'l_knee_y'])

##Bras gauche du robot poppy

#Fonction du modèle geometrique direct avec définition des arguments
def MGD_l (theta1, theta2, theta3, theta4):
    a1=0.0771
    a2=0.05
    a3=0.004
    a4=0.0284
    a5=0.03625
    a6=0.0185
    a7=0.11175
    a8=-0.01
    a9=0.15

#Calcul des coordonnées dans l'espace opérationnel
    x_l =(cos(theta2)*cos(theta4)+sin(theta2)*sin(theta3)*sin(theta4))*a9 + cos(theta2)*a7 + sin(theta2)*sin(theta3)*a8 + cos(theta2)*a5 + a4 + a1
    
    y_l=(-sin(theta1)*sin(theta2)*cos(theta4)+(sin(theta1)*cos(theta2)*sin(theta3)-cos(theta1)*cos(theta3))*sin(theta4))*a9 - sin(theta1)*sin(theta2)*a7 + (sin(theta1)*cos(theta2)*sin(theta3)-cos(theta1)*cos(theta3))*a8 - sin(theta1)*sin(theta2)*a5-cos(theta1)*a6+a2
    
    z_l=(cos(theta1)*sin(theta2)*cos(theta4)+(-cos(theta1)*cos(theta2)*sin(theta3)-sin(theta1)*cos(theta3))*sin(theta4))*a9 + cos(theta1)*sin(theta2)*a7 + (-cos(theta1)*cos(theta2)*sin(theta3)-sin(theta1)*cos(theta3))*a8 + cos(theta1)*sin(theta2)*a5 - sin(theta1)*a6 + a3
    
    X_l=np.array([[x_l], [y_l], [z_l]])
 
    return X_l
    
#Fonction du modèle cinématique direct
def Jacob_l(theta1,theta2, theta3, theta4):
    a1=0.0771
    a2=0.05
    a3=0.004
    a4=0.0284
    a5=0.03625
    a6=0.0185
    a7=0.11175
    a8=-0.01
    a9=0.15
    
    #Calcul des jacobiennes (Matrices de Jacobi)
    J11=0
    
    J12 = (-sin(theta2)*cos(theta4) + cos(theta2)*sin(theta3)*sin(theta4))*a9 -sin(theta2)*a7 + cos(theta2)*sin(theta3)*a8 - sin(theta2)*a5
    
    J13 = sin(theta2)*cos(theta3)*sin(theta4)*a9 + sin(theta2)*cos(theta3)*a8
    
    J14 = (-cos(theta2)*sin(theta4)+ sin(theta2)*sin(theta3)*cos(theta4))*a9
         
    J21 = (-cos(theta1)*sin(theta2)*cos(theta4) + (cos(theta1)*cos(theta2)*sin(theta3) + sin(theta1)*cos(theta3))*sin(theta4))*a9 - cos(theta1)*sin(theta2)*a7 + (cos(theta1)*cos(theta2)*sin(theta3) + sin(theta1)*cos(theta3))*a8 -cos(theta1)*sin(theta2)*a5 + sin(theta1)*a6
         
    J22 = (-sin(theta1)*cos(theta2)*cos(theta4)-sin(theta1)*sin(theta2)*sin(theta3)*sin(theta4))*a9 - sin(theta1)*cos(theta2)*a7 - sin(theta1)*sin(theta2)*sin(theta3)*a8 - sin(theta1)*cos(theta2)*a5

    J23 = (sin(theta1)*cos(theta2)*cos(theta3) + cos(theta1)*sin(theta3))*sin(theta4)*a9 + (sin(theta1)*cos(theta2)*cos(theta3)+cos(theta1)*sin(theta3))*a8
    
    J24 = (sin(theta1)*sin(theta2)*sin(theta4) + (sin(theta1)*cos(theta2)*sin(theta3)-cos(theta1)*cos(theta3))*cos(theta4))*a9

    J31 = (-sin(theta1)*sin(theta2)*cos(theta4) + (sin(theta1)*cos(theta2)*sin(theta3) - cos(theta1)*cos(theta3))*sin(theta4))*a9 - sin(theta1)*sin(theta2)*a7 + (sin(theta1)*cos(theta2)*sin(theta3) - cos(theta1)*cos(theta3))*a8 - sin(theta1)*sin(theta2)*a5 - cos(theta1)*a6 
    
    J32 = (cos(theta1)*cos(theta2)*cos(theta4) + cos(theta1)*sin(theta2)*sin(theta3)*sin(theta4))*a9 + cos(theta1)*cos(theta2)*a7 + cos(theta1)*sin(theta2)*sin(theta3)*a8 + cos(theta1)*cos(theta2)*a5

    J33 = (-cos(theta1)*cos(theta2)*cos(theta3) + sin(theta1)*sin(theta3))*sin(theta4)*a9 + (-cos(theta1)*cos(theta2)*cos(theta3)+ sin(theta1)*sin(theta3))*a8

    J34 = (-cos(theta1)*sin(theta2)*sin(theta4)+ (-cos(theta1)*cos(theta2)*sin(theta3) - sin(theta1)*cos(theta3))*cos(theta4))*a9
    
    J=np.array([[J11, J12, J13, J14],[J21, J22, J23, J24],[J31, J32, J33, J34]])
    
    return J

if __name__ == '__main__' :
    #Etablissement de la connection avec Vrep ou CoppeliaSim
    poppy = vrep_io.VrepIO('127.0.0.1', 19997,'poppy_humanoid.ttt')

    #Démarrage de la simulation
    poppy.start_simulation()
    
    #Déclaration des angles et variables

    j=0
    
    N=80000
    
    XX=np.zeros((3,N))
    
    XXX=np.zeros((3,N))
    
    dT=0.1#seconde
     
    l_arm = ['l_shoulder_y', 'l_shoulder_x', 'l_arm_z', 'l_elbow_y']
    
    theta1=poppy.get_motor_position(l_arm[0])
        
    theta2=poppy.get_motor_position(l_arm[1])
        
    theta3=poppy.get_motor_position(l_arm[2])
        
    theta4=poppy.get_motor_position(l_arm[3])
        
    Xi=MGD_l(theta1, theta2, theta3, theta4)
    
    Xf= np.array([[0.2535], [0.1915], [0.004]])
    
    D = Xf - Xi
    
##Commande cinématique et génération de trajectoire dans l'espace opérationnel du robot

    #Définition de la position articulaire désirée initiale
    qd=np.zeros((4,1))
   
    #Définition du temps 
    tf = 40 #seconde
    
    #Définition des articulations du bras 
    l_arm = ['l_shoulder_y', 'l_shoulder_x', 'l_arm_z', 'l_elbow_y']
    
    #Définition du clock en syncronisation avec vrep
    t0= vrep_poppy.Time(poppy).time()
    
    while vrep_poppy.Time(poppy).time()-t0<tf:
        
          t=vrep_poppy.Time(poppy).time()-t0
          
         
        #Définition de r(t)
          r = 10 *(t/tf)**3 - 15 *(t/tf)**4 + 6 *(t/tf)**5
    
        #Définition de la position désirée du robot
          Xd = Xi + r*D   
          
          XX[0,j]=Xd[0]
          
          XX[1,j]=Xd[1]
          
          XX[2,j]=Xd[2]
          
          #Gain proportionnel
          K=np.diag([5, 5, 0.05])
        
        #Définition de l'erreur entre la position désirée et la position courante du robot 
          a=poppy.get_motor_position(l_arm[0])
        
          b=poppy.get_motor_position(l_arm[1])
        
          c=poppy.get_motor_position(l_arm[2])
        
          d=poppy.get_motor_position(l_arm[3])
        
          Xc=MGD_l(a, b, c, d)
          
          XXX[0,j]=Xc[0]
          
          XXX[1,j]=Xc[1]
          
          XXX[2,j]=Xc[2]
          
          j=j+1
         
         
        #Calcul de l'erraur entre la position désirée et la position courante
          Ep = Xd - Xc
          
        #Correction de l'erreur par le correcteur proportionnel
          Xpointd = K @ Ep
         
        #Calcul de la jacobienne inverse
          Jp=np.linalg.pinv(Jacob_l(a, b, c, d))
          
        #Passage de l'espace opérationnel à l'espace articulaire
        
          qpointd = Jp @ Xpointd
          
        #Intégration de la matrice de position articulaire 
         
          qd[0] = qpointd[0]*dT + a
          
          qd[1] = qpointd[1]*dT + b
          
          qd[2] = qpointd[2]*dT + c
          
          qd[3] = qpointd[3]*dT + d
        
        #Ecriture des positions articulaires désirées : Envoyer le bras vers une position
              
          poppy.set_motor_position(l_arm[0], qd[0])  
          
          poppy.set_motor_position(l_arm[1], qd[1])  
          
          poppy.set_motor_position(l_arm[2], qd[2])
          
          poppy.set_motor_position(l_arm[3], qd[3])  
       
        #Recupération de la postion articulaire
          for i in range(len(l_arm)):
            
              q=poppy.get_motor_position(l_arm[i])
              
              
              
              
              
              

###Bras droit du robot Poppy

#          #Fonction du modèle geometrique direct avec définition des arguments
# def MGD_r (t1, t2, t3, t4):
#     a1=0.0771
#     a2=0.05
#     a3=0.004
#     a4=0.0284
#     a5=0.03625
#     a6=0.0185
#     a7=0.11175
#     a8=-0.01
#     a9=0.15

# #Calcul des coordonnées dans l'espace opérationnel
#     x_r =(cos(t2)*cos(t4)+sin(t2)*sin(t3)*sin(t4))*a9 + cos(t2)*a7 + sin(t2)*sin(t3)*a8 + cos(t2)*a5 + a4 + a1
    
#     y_r=(-sin(t1)*sin(t2)*cos(t4)+(sin(t1)*cos(t2)*sin(t3)-cos(t1)*cos(t3))*sin(t4))*a9 - sin(t1)*sin(t2)*a7 + (sin(t1)*cos(t2)*sin(t3)-cos(t1)*cos(t3))*a8 - sin(t1)*sin(t2)*a5-cos(t1)*a6+a2
    
#     z_r=(cos(t1)*sin(t2)*cos(t4)+(-cos(t1)*cos(t2)*sin(t3)-sin(t1)*cos(t3))*sin(t4))*a9 + cos(t1)*sin(t2)*a7 + (-cos(t1)*cos(t2)*sin(t3)-sin(t1)*cos(t3))*a8 + cos(t1)*sin(t2)*a5 - sin(t1)*a6 + a3
    
#     X_r=np.array([[x_r], [y_r], [z_r]])
 
#     return X_r
    
# #Fonction du modèle cinématique direct
# def Jacob_r (t1,t2, t3, t4):
#     a1=0.0771
#     a2=0.05
#     a3=0.004
#     a4=0.0284
#     a5=0.03625
#     a6=0.0185
#     a7=0.11175
#     a8=-0.01
#     a9=0.15
    
#     #Calcul des jacobiennes (Matrices de Jacobi)
#     J11_r =0
    
#     J12_r = (-sin(t2)*cos(t4) + cos(t2)*sin(t3)*sin(t4))*a9 -sin(t2)*a7 + cos(t2)*sin(t3)*a8 - sin(t2)*a5
    
#     J13_r = sin(t2)*cos(t3)*sin(t4)*a9 + sin(t2)*cos(t3)*a8
    
#     J14_r = (-cos(t2)*sin(t4)+ sin(t2)*sin(t3)*cos(t4))*a9
         
#     J21_r = (-cos(t1)*sin(t2)*cos(t4) + (cos(t1)*cos(t2)*sin(t3) + sin(t1)*cos(t3))*sin(t4))*a9 - cos(t1)*sin(t2)*a7 + (cos(t1)*cos(t2)*sin(t3) + sin(t1)*cos(t3))*a8 - cos(t1)*sin(t2)*a5 + sin(t1)*a6
         
#     J22_r = (-sin(t1)*cos(t2)*cos(t4)-sin(t1)*sin(t2)*sin(t3)*sin(t4))*a9 - sin(t1)*cos(t2)*a7 - sin(t1)*sin(t2)*sin(t3)*a8 - sin(t1)*cos(t2)*a5

#     J23_r = (sin(t1)*cos(t2)*cos(t3) + cos(t1)*sin(t3))*sin(t4)*a9 + (sin(t1)*cos(t2)*cos(t3)+cos(t1)*sin(t3))*a8
    
#     J24_r = (sin(t1)*sin(t2)*sin(t4) + (sin(t1)*cos(t2)*sin(t3)-cos(t1)*cos(t3))*cos(t4))*a9

#     J31_r = (-sin(t1)*sin(t2)*cos(t4) + (sin(t1)*cos(t2)*sin(t3) - cos(t1)*cos(t3))*sin(t4))*a9 - sin(t1)*sin(t2)*a7 + (sin(t1)*cos(t2)*sin(t3) - cos(t1)*cos(t3))*a8 - sin(t1)*sin(t2)*a5 - cos(t1)*a6 
    
#     J32_r = (cos(t1)*cos(t2)*cos(t4) + cos(t1)*sin(t2)*sin(t3)*sin(t4))*a9 + cos(t1)*cos(t2)*a7 + cos(t1)*sin(t2)*sin(t3)*a8 + cos(t1)*cos(t2)*a5

#     J33_r = (-cos(t1)*cos(t2)*cos(t3) + sin(t1)*sin(t3))*sin(t4)*a9 + (-cos(t1)*cos(t2)*cos(t3)+ sin(t1)*sin(t3))*a8

#     J34_r = (-cos(t1)*sin(t2)*sin(t4)+ (-cos(t1)*cos(t2)*sin(t3) - sin(t1)*cos(t3))*cos(t4))*a9
    
#     J_r =np.array([[J11_r, J12_r, J13_r, J14_r],[J21_r, J22_r, J23_r, J24_r],[J31_r, J32_r, J33_r, J34_r]])
    
#     return J_r

# if __name__ == '__main__' :
    
#     #Déclaration des angles et variables

#     j=0
    
#     N=80000
  
#     dT=0.1#seconde
     
#     r_arm = ['r_shoulder_y', 'r_shoulder_x', 'r_arm_z', 'r_elbow_y']
    
#     t1=poppy.get_motor_position(r_arm[0])
        
#     t2=poppy.get_motor_position(r_arm[1])
        
#     t3=poppy.get_motor_position(r_arm[2])
        
#     t4=poppy.get_motor_position(r_arm[3])
        
#     Xi_r =MGD_r(t1, t2, t3, t4)
    
#     Xf_r = np.array([[0.2535], [0.1915], [0.004]])
    
#     D_r = Xf_r - Xi_r
    
# ##Commande cinématique et génération de trajectoire dans l'espace opérationnel du robot

#     #Définition de la position désirée initiale
#     qd_r=np.zeros((4,1))
   
#     #Mémorisation de la position désirée qd dans une matrice q_d
    
#     # q_d_r = np.zeros((4,N))
    
#     #Définition du temps 
#     tf = 40 #seconde
    
#     #Définition des articulations du bras 
#     r_arm = ['r_shoulder_y', 'r_shoulder_x', 'r_arm_z', 'r_elbow_y']
    
#     #Définition du clock en syncronisation avec vrep
#     t0= vrep_poppy.Time(poppy).time()
    
#     while vrep_poppy.Time(poppy).time()-t0<tf:
        
#           t=vrep_poppy.Time(poppy).time()-t0
          
         
#         #Définition de r(t)
#           r_r = 10 *(t/tf)**3 - 15 *(t/tf)**4 + 6 *(t/tf)**5
    
#         #Définition de la position désirée du robot
#           Xd_r = Xi_r + r_r*D_r   
          
#           K_r=np.diag([5, 5, 0.05])
        
#         #Définition de l'erreur entre la position désirée et la position courante du robot 
#           a_r=poppy.get_motor_position(r_arm[0])
        
#           b_r=poppy.get_motor_position(r_arm[1])
        
#           c_r=poppy.get_motor_position(r_arm[2])
        
#           d_r=poppy.get_motor_position(r_arm[3])
        
#           Xc_r=MGD_r(a_r, b_r, c_r, d_r)
                
#           j=j+1
         
         
#        #Calcul de l'erraur entre la position désirée et la position courante
#           Ep_r = Xd_r - Xc_r
          
#         #Correction de l'erreur par le correcteur proportionnel
#           Xpointd_r = K_r @ Ep_r
         
#         #Calcul de la jacobienne inverse
#           Jp_r=np.linalg.pinv(Jacob_r(a_r, b_r, c_r, d_r))
          
#         #Passage de l'espace opérationnel à l'espace articulaire
        
#           qpointd_r = Jp_r @ Xpointd_r
          
#         #Intégration de la matrice de position articulaire 
         
#           qd_r[0] = qpointd_r[0]*dT + a_r
          
#           qd_r[1] = qpointd_r[1]*dT + b_r
          
#           qd_r[2] = qpointd_r[2]*dT + c_r
          
#           qd_r[3] = qpointd_r[3]*dT + d_r
        
#         #Ecriture des positions articulaires désirées : Envoyer le bras vers une position
              
#           poppy.set_motor_position(r_arm[0], qd_r[0])  
          
#           poppy.set_motor_position(r_arm[1], qd_r[1])  
          
#           poppy.set_motor_position(r_arm[2], qd_r[2])
          
#           poppy.set_motor_position(r_arm[3], qd_r[3])  
       
#         #Recupération de la postion articulaire
#           for i in range(len(r_arm)):
            
#               q=poppy.get_motor_position(r_arm[i])


